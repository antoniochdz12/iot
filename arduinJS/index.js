const express = require('express');
const { Board, Led, Button, Proximity, Piezo } = require("johnny-five");
const app = express();
const board = new Board();
let led, button, button2, buzzer, proximity;
const os = require('os');

const PORT = 3000; // Puerto en el que tu servidor está escuchando

app.listen(PORT, () => {
  const interfaces = os.networkInterfaces();
  for (let iface of Object.values(interfaces)) {
    for (let alias of iface) {
      if (alias.family === 'IPv4' && !alias.internal) {
        console.log(`Servidor iniciado en http://${alias.address}:${PORT}`);
      }
    }
}
});

board.on("ready", () => {
  led = new Led(13);
  button = new Button(2);
  button2 = new Button(3);
  buzzer = new Piezo(11);
  proximity = new Proximity({
    controller: "HCSR04",
    pin: 5,
    freq: 50 // Establece la frecuencia de emisión de eventos de datos
  });

  // Lógica para botones y sensor de proximidad
  button.on("down", () => console.log("Trabajando máquina 1"));
  button2.on("down", () => {
    console.log("Emergencia");
    buzzer.frequency(1000, 1000); // Hace sonar el buzzer a 1000Hz por 1000ms
  });
  button2.on("up", () => buzzer.off()); // Apaga el buzzer cuando se suelta el botón

  // Lógica para encender el LED basado en la proximidad del objeto
  proximity.on("data", () => {
    if (proximity.cm < 20) {
      led.on();
      console.log("Persona frente a máquina");
    } else {
      led.off();
    }
  });
});

app.get('/api/led/on', (req, res) => {
  if (led) {
    led.on();
    res.json({ status: 'LED encendido' });
  } else {
    res.status(500).json({ error: 'Board no está listo' });
  }
});

app.get('/api/led/off', (req, res) => {
  if (led) {
    led.off();
    res.json({ status: 'LED apagado' });
  } else {
    res.status(500).json({ error: 'Board no está listo' });
  }
});

app.get('/api/proximity', (req, res) => {
  if (proximity) {
    res.json({ distance: proximity.cm });
  } else {
    res.status(500).json({ error: 'Sensor no está listo' });
  }
});

app.listen(3000, () => console.log('Servidor iniciado en el puerto 3000'));
